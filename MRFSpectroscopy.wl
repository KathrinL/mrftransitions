(* ::Package:: *)

(* ::Title:: *)
(*Package*)


BeginPackage["MRFSpectroscopy`"]


FindTransitions::usage =
        "FindTransitions[dressingRFs,rabiFrequencies,freqSep] finds all transitions for atoms trapped in an MRF-dressed potential with the given parameters, probes by the RF probe field specified.";
        
FindFirstOrderTransitions::usage = "FindFirstOrderTransitions[dressingRFs, rabiFrequencies, freqSep, mf] finds all first-order transitions to the final m_F state mf in the specified MRF protential.";

FindAnyOrderTransitions::usage = "FindAnyOrderTransitions[dressingRFs, rabiFrequencies, freqSep, mf, order] finds all transitions of order order in the probe field to the final m_F state mf in the specified MRF protential.";

FindBasis::usage = "FindBasis[dressingRFs, rabiFrequencies, freqSep] finds the basis which diagonalises the Hamiltonian and corresponding eigenvalues with the given dressing-RF field at the energy separation of internal field, specified.";

FindDetuning::usage = "FindDetuning[dressingRFs, rabiFrequencies, freqSep] finds the detuning for which the atoms are at the minimum of the potential including gravity and for the given dressing-RF field, with freqSep as a starting point.";

PlotTransitions::usage = "PlotTransitions[transitions] plots all transitions (as returned by one of the FindTransitions methods";

WriteToFile::usage = "WriteToFile[transitions, filePath] writes the transitions into a file with the specified path";


Begin["`Private`"]


(* ::Chapter:: *)
(*Main Functions*)


(* ::Text:: *)
(*The options for all methods are given globally. This does mean that some options are redundant for some methods.*)


options = {Debug -> False, F -> 1, mF -> 1, probeInteraction -> "arbitrary", polarisation -> "circularMinus", nMax -> 10, omegaf -> 1, threshold -> 10^(-13), 
	OmegazAttenuation -> 0, shift -> 0, signgF -> -1, quadGrad -> 0};


(* ::Text:: *)
(**)


(* ::Text:: *)
(*Transitions are returned in the following form: {{order, m_initial, m_final},{{amp, omegap(omegarf,Omega), omegap(omegarf,Omega)},...,{amp, omegap(omegarf,Omega), omegap(omegarf,Omega)}}}.*)
(*Where the first instance of omegap(omegarf,Omega) is the numerical value for the transition frequency, and the second instance is the transition frequency as a function of omegarf and Omega*)


Options[FindTransitions] = options;

FindTransitions[dressingRFs_, rabiFrequencies_, freqSep_, maxOrder_, mStates_, opt:OptionsPattern[]]:=Module[{mF = OptionValue[mF],
																						transitions, basis, mf, o, values},
	If[!checkInputs[dressingRFs, rabiFrequencies, freqSep, opt], Return[$Failed]];
	basis = FindBasis[dressingRFs, rabiFrequencies, freqSep, opt];
	values = basis[[2]];
	basis = basis[[1]];
	transitions = {};
	(*find all transitions*)
	Do[
		mf = mStates[[i]];
		Do[
			Print["order: ", o, ", final state: ", mf];
			transitions = Append[transitions, {{o, mF, mf}, anyOrder[mf, basis, values, o, opt]}],
			{o, 1, maxOrder}],
	{i, 1, Length[mStates]}];
	transitions
]



Options[FindAnyOrderTransitions] = options;

FindAnyOrderTransitions::wrongmF = "mf needs to be a number between -F and F, with integer difference between F and mf!";

FindAnyOrderTransitions[dressingRFs_, rabiFrequencies_, freqSep_, mf_, order_, opt:OptionsPattern[]]:=Module[{mF = OptionValue[mF], F = OptionValue[F], quadGrad = OptionValue[quadGrad],
																									basis, transitions, values, detuning},
	If[!checkInputs[dressingRFs, rabiFrequencies, freqSep, opt], Return[$Failed]];
	If[Abs[mf]>F||!IntegerQ[F-mf], Message[FindAnyOrderTransitions::wrongmF, Style[FindAnyOrderTransitions, Red]]; Return[$Failed], True, Message[FindAnyOrderTransitions::wrongmF, Style[FindAnyOrderTransitions, Red]]; Return[$Failed]];
	If[quadGrad > 0,
		detuning = FindDetuning[dressingRFs, rabiFrequencies, freqSep, opt],
		detuning = freqSep
	];
	basis = FindBasis[dressingRFs, rabiFrequencies, detuning, opt];
	values = basis[[2]];
	basis = basis[[1]];
	transitions = anyOrder[mf, basis, values, order, opt];
	transitions = {{{order, mF, mf}, transitions}}
]


Options[FindBasis] = options;

FindBasis[dressingRFs_, rabiFrequencies_, freqSep_, opt:OptionsPattern[] ] := Module[{debug = OptionValue[Debug], F = OptionValue[F], nmax = OptionValue[nMax], omegaf = OptionValue[omegaf],
																					H, evals, omega0, evecs, basis, ylim, threshold = OptionValue[threshold], values},
	If[!checkInputs[dressingRFs, rabiFrequencies, freqSep, opt], Return[$Failed]];
	H=FindHamiltonian[dressingRFs, rabiFrequencies, omega0, opt];
	If[debug, (*in debug mode, eigenvalues of the Hamiltonian are plotted and a line for specified energy splitting freqSep is added*)
		ylim = Max[omegaf (dressingRFs+2)];
		evals=Table[Sort[Eigenvalues[H/.omega0 -> delta]],{delta,-ylim,ylim,1/100}];
		Print["Eigenvalues"];
		Print[ListLinePlot[Transpose[evals], DataRange -> {-ylim,ylim}, PlotRange -> {All,{-ylim,ylim}}, GridLines->{{freqSep},{}}, GridLinesStyle->Directive[Thick, Dashed]]]
	];
	{evals, evecs} = Eigensystem[H/.omega0 -> freqSep];
	(*ordering eigenvalues and eigenvectors by magnitude of eigenvalue*)
	{evals, evecs} ={evals[[#]],evecs[[#]]}&@Ordering[evals];
	If[debug, (*in debug mode, we find the transformation matrix U and check that it indeed diagonalises H*)
		Print["Diagonalised Hamiltonian, Unitary Transformation Matrix and U.U^dagger - Identity"];
		Print[Row[{
			ArrayPlot[evecs.H.Transpose[evecs]/.omega0 -> freqSep,ImageSize->Medium,Mesh->All],
			ArrayPlot[Transpose[evecs],ImageSize->Medium,Mesh->All],
			ArrayPlot[Transpose[evecs].evecs-IdentityMatrix[2*(2 F+1) nmax+(2F+1)],ImageSize->Medium,Mesh->All]}]]
	];	
	(*we choose 2F+1 eigenvectors corresponding to the different mF tilde states. choose them in the middle with roughly equal contribution from either side (i.e. around N = 0*)
	basis = ConstantArray[0,{2F+1,Length[evecs]}];
	values = ConstantArray[0,{2F+1}];
	Do[basis[[i]]=evecs[[F(2F+1)+Floor[(nmax-F)]*(2F+1)+i]]; values[[i]]=evals[[F(2F+1)+Floor[(nmax-F)]*(2F+1)+i]],{i,1,2F+1}];
	If[debug,
		Print["Chosen eigenvectors"];
		Print[ArrayPlot[basis,Mesh->All]];
		Print[basis//MatrixForm];
		Print[values];
	];
	basis = Threshold[basis,threshold];
	basis[[2]] = -basis[[2]];
	basis = {basis, values};
	basis
]

Options[FindDetuning] = options;

FindDetuning[dressingRFs_, rabiFrequencies_, freqSep_, opt:OptionsPattern[] ] := Module[{F = OptionValue[F], nmax = OptionValue[nMax], quadGrad = OptionValue[quadGrad],
																					omegaf = OptionValue[omegaf], mF = OptionValue[mF],
																					H, evals, omega0, g, h, m, lim1, lim2, Delta, delta, continue = True,
																					min, minPos, pos, delta2, evals2, minPos2, counter = 1},
	Print["Warning: Finding the minimum currently only works for Rb-87 atoms in F = 1, that is for atoms with gF = -1/2, and the method will return erroneous results for other species!"];
	If[!checkInputs[dressingRFs, rabiFrequencies, freqSep, opt], Return[$Failed]];
	If[quadGrad == 0, Print["Quadrupole Gradient was set to zero. Returning starting value for detuning."]; Return[freqSep]];
	(*relevant constants*)
	g = 9.81; (*m/s^2*)
	m = 87*1.660539*10^(-27);
	h = 6.626*10^(-34);
	
	H=FindHamiltonian[dressingRFs, rabiFrequencies, omega0, opt];
	
	Delta = omegaf/5;
	delta = Delta/100;
	delta2 = delta/100;
	pos = freqSep;
	While[ continue, (*finding the minimum of a trapped eigenstate in the range pos-Delta, pos+Delta. If the minimum is at the edges, repeat*)
		counter = counter+1;
		evals=Table[Sort[Eigenvalues[H/.omega0 -> freq]] - m*g*freq*0.01 / (2*0.7*quadGrad*h*10^6),{freq,pos-Delta,pos+Delta,delta}];
		min = Min[evals[[;;,F(2F+1)+Floor[(nmax-F)]*(2F+1)+2+mF]]];
		minPos = FirstPosition[evals[[;;,F(2F+1)+Floor[(nmax-F)]*(2F+1)+2+mF]],min];
		minPos = pos - Delta + (minPos[[1]]-1) * delta;
		If[minPos == pos - Delta,
			pos = pos - Delta,
			If[minPos == pos + Delta,
				pos = pos + Delta,
				continue = False
				];
		];
		If[counter >= 100, 
			Print["Warning: no minimum was found within 100 iterations. Search has been abandoned, and initial value returned"];
			minPos = freqSep;
			continue = False
		];	
	];
	
	If[counter < 100,	
		(*Find the minimum in a small region around the previous one with finer spacing*)
		evals2 = Table[Sort[Eigenvalues[H/.omega0 -> freq]] - m*g*freq*0.01 / (2*0.7*quadGrad*h*10^6),{freq,minPos-delta,minPos+delta,delta2}];
		min = Min[evals2[[;;,F(2F+1)+Floor[(nmax-F)]*(2F+1)+2+mF]]];
		minPos2 = FirstPosition[evals2[[;;,F(2F+1)+Floor[(nmax-F)]*(2F+1)+2+mF]],min];
		minPos2 = minPos - delta + (minPos2[[1]]-1) * delta2,
		minPos2 = minPos
	];

	Print["Eigenenergies, initial detuning and minimum found indicated by vertical dashed lines:"];
	lim2 = Max[{freqSep+2 omegaf,minPos2+2 omegaf}];
	lim1 = Min[{freqSep-2 omegaf,minPos2-2 omegaf}];
	evals = Table[Sort[Eigenvalues[H/.omega0 -> freq]] - m*g*freq*0.01 / (2*0.7*quadGrad*h*10^6),{freq,lim1,lim2,10*delta}];
	Print[ListLinePlot[Transpose[evals[[;;,F(2F+1)+Floor[(nmax-F)]*(2F+1)+1;;F(2F+1)+Floor[(nmax-F)]*(2F+1)+3]]], DataRange -> {lim1,lim2}, PlotRange -> {All,All}, GridLines->{{minPos2,freqSep},{}}, GridLinesStyle->Directive[Thick, Dashed]]];
	minPos2
]

Options[PlotTransitions] = {PlotRange->{{0,10},Full},Frame->True,FrameTicksStyle->18}; 

PlotTransitions[transitions_, opt:OptionsPattern[] ] := Module[{spectrum = {}, i, spec, legend = {}},
	Do[
		spec = makeSpectrum[ transitions[[i,2]] ];
		If[spec != {},
			spectrum = Append[spectrum, spec];
			legend = Append[legend, StringJoin["Order: " , ToString[transitions[[i,1,1]]], ", \!\(\*SubscriptBox[\(m\), \(F\)]\)~ = ", ToString[transitions[[i,1,2]]], "-> \!\(\*SubscriptBox[\(m\), \(F\)]\)~ = ", ToString[transitions[[i,1,3]]]]]
		], 
		{i, 1, Length[transitions]}
	];
	ListLinePlot[spectrum,PlotLegends->legend,opt, #]&@Options[PlotTransitions]
]


(* ::Chapter:: *)
(*Private Functions*)
(*Basic Operators*)
(*Hamiltonian*)


(* ::Text:: *)
(*FindHamiltonian[dressingRFs, rabiFrequencies, freqSep] returns a matrix for the dressed Hamiltonian with parameters as specified.*)


Options[FindHamiltonian] = options;

FindHamiltonian[dressingRFs_, rabiFrequencies_, freqSep_, OptionsPattern[] ] := Module[{F = OptionValue[F], polarisation = OptionValue[polarisation], nmax = OptionValue[nMax], omegaf = OptionValue[omegaf], OmegazAttenuation = OptionValue[OmegazAttenuation], 
																						shift = OptionValue[shift], signgF = OptionValue[signgF],
																						basis, omega0, H0, Vlin, VcircPlus, VcircMinus, Vlinz, H, a, aDagger, idField, photonNumber, sigmaPlus, sigmaMinus, sigmaZ, sigmaZd, idAtom, V},
	(*fields*)
	a[i_,j_,Delta_]:=Boole[basis[[i,1]]==basis[[j,1]]-Delta];
	aDagger[i_,j_,Delta_]:=Boole[basis[[i,1]]==basis[[j,1]]+Delta];
	idField[i_,j_]:=Boole[basis[[i,1]]==basis[[j,1]]];
	photonNumber[i_,j_]:=basis[[i,1]]idField[i,j];
	(*atoms*)
	sigmaPlus[i_,j_]:=Boole[basis[[i,2]]==basis[[j,2]]+1](F(F+1)-basis[[j,2]]basis[[i,2]])^(1/2);
	sigmaMinus[i_,j_]:=Boole[basis[[i,2]]==basis[[j,2]]-1](F(F+1)-basis[[j,2]]basis[[i,2]])^(1/2);
	sigmaZ[i_,j_]:=Boole[basis[[i,2]]==basis[[j,2]]]basis[[i,2]];
	sigmaZd[i_,j_]:=Boole[basis[[i,2]]==basis[[j,2]]]If[basis[[i,2]]>=0,basis[[i,2]],basis[[i,2]]-shift/omega0];
	idAtom[i_,j_]:=Boole[basis[[i,2]]==basis[[j,2]]];
	(*Hamiltonian*) 
	basis = Flatten[Table[{N,m},{N,nmax,-nmax,-1},{m,F,-F,-1}],1];
	H0[i_,j_] := photonNumber[i,j] idAtom[i,j]omegaf+signgF idField[i,j]omega0 sigmaZd[i,j];
	Vlin[i_,j_,Omega_,Delta_] := Omega/2 (sigmaPlus[i,j]a[i,j,Delta]+sigmaMinus[i,j]aDagger[i,j,Delta]+sigmaPlus[i,j]aDagger[i,j,Delta]+sigmaMinus[i,j]a[i,j,Delta]); (*linear polarised along x*)
	VcircPlus[i_,j_,Omega_,Delta_] := Omega/2 (sigmaPlus[i,j]a[i,j,Delta]+sigmaMinus[i,j]aDagger[i,j,Delta]);
	VcircMinus[i_,j_,Omega_,Delta_] := Omega/2 (sigmaPlus[i,j]aDagger[i,j,Delta]+sigmaMinus[i,j]a[i,j,Delta]);
	Vlinz[i_,j_,Omega_,Delta_]:=Omega/2 (sigmaPlus[i,j]a[i,j,Delta]+sigmaMinus[i,j]aDagger[i,j,Delta]+sigmaPlus[i,j]aDagger[i,j,Delta]+sigmaMinus[i,j]a[i,j,Delta]+OmegazAttenuation sigmaZ[i,j]a[i,j,Delta]+OmegazAttenuation sigmaZ[i,j]aDagger[i,j,Delta]); (*linear polarised along x with some element of sigma z*)
	Which[
			polarisation == "circularPlus", V[i_,j_] := Sum[VcircPlus[i,j,rabiFrequencies[[n]], dressingRFs[[n]]],{n,1,Length[rabiFrequencies]}],
			polarisation == "circularMinus", V[i_,j_] := Sum[VcircMinus[i,j,rabiFrequencies[[n]], dressingRFs[[n]]],{n,1,Length[rabiFrequencies]}],
			polarisation == "linear", V[i_,j_] := Sum[Vlin[i,j,rabiFrequencies[[n]], dressingRFs[[n]]],{n,1,Length[rabiFrequencies]}],
			polarisation == "linearZ", V[i_,j_] := Sum[Vlinz[i,j,rabiFrequencies[[n]], dressingRFs[[n]]],{n,1,Length[rabiFrequencies]}]
		];
	H = Table[H0[i,j]+V[i,j],{i, 1, 2(2F+1)nmax + (2F+1)},{j, 1, 2(2F+1)nmax + (2F+1)}];
	H/.{omega0 -> freqSep}(*this does not plug in any value for omega0 but makes sure that the Hamiltonian is returned with a variable omega0 for the frequency that's local to the function from which this one was called and can be replaced with a number later (rather than it being a variable local to this method)*)
]


(* ::Section:: *)
(*Matrix Elements*)


Options[UdVU] = options;

(*This method returns the matrix element < n2, m2 | U^dagger Vp U | n1, m1 > as a functin of nf, np for the probe polarisation which was specified. *)
(*The matrix element is returned as a sum of Kronecker deltas, specified as a list. For details see notes at beginning of Section "Helper Function".*)
UdVU[m1_,m2_, basis_, n1_, n2_, nf_, np_, OptionsPattern[] ]:=Module[{nmax = OptionValue[nMax], F= OptionValue[F], probeInteraction = OptionValue[probeInteraction], threshold = OptionValue[threshold],
						Vp, matrixElem, rule},
	Which[ 
		probeInteraction == "arbitrary", Vp = Vparb,
		probeInteraction == "linearX", Vp = Vplinx,
		probeInteraction == "linearZ", Vp = Vpz,
		probeInteraction == "circularPlus", Vp = VpcircPlus,
		probeInteraction == "circularMinus", Vp = VpcircMinus,
		probeInteraction == "amplitudes", Vp = VpAmps,
		probeInteraction == "experiment", Vp = VpExp
	];	
	matrixElem = Flatten[Table[Table[Table[Table[ (*Sum_{mi, mj, i, j} <n2 + j*nf, mj| Vp | n1 + i*nf, mi>, i.e. this is the matrix element <n2, m2 | U^dagger Vp U | n1, m1 >*)
		scalarProduct[{{Conjugate[basis[[m2+1+F,(2 F+1) (nmax-j)-mj+1+F]]],n2+j nf,mj}},Vp[{{basis[[m1+1+F,(2F+1) (nmax-i)-mi+1+F]],n1+i nf, mi}},np, F]],
		{j,-nmax,nmax}],{i,-nmax,nmax}],{mi,-F,F}],{mj,-F,F}],4];
	matrixElem = makeState[matrixElem];
	Do[ (*rephrasing the Kronecker deltas as delta(n1-n2, (n1-n2)(nf, np)*)
		rule = Flatten[Solve[matrixElem[[j,2]]==matrixElem[[j,3]],n1]];
		matrixElem[[j,2]]=n1-n2;
		matrixElem[[j,3]]=n1-n2/.rule,
	{j,1,Length[matrixElem]}];
	matrixElem = makeState[matrixElem];
	matrixElem[[;;,1]]=Chop[matrixElem[[;;,1]],threshold];
	makeState[matrixElem]
]


Options[firstOrder] = options;

firstOrder[m2_, basis_, values_, opt:OptionsPattern[] ] := Module[{threshold = OptionValue[threshold], m1 = OptionValue[mF], F = OptionValue[F], omegafundamental = OptionValue[omegaf],
																matrixElem, n1, n2, nf, np, npRule, omegafp, Omega, omegaf, omegap},
	matrixElem = UdVU[m1, m2, basis, n1, n2, nf, np, opt]; (*<n2, m2|U^dagger V U|n1, m1>*)
	Do[ (*omegafp here is the new fundamental frequency (omegaf prime, or omegaf probe)*)
		npRule = Flatten[Solve[matrixElem[[j,2]]==matrixElem[[j,3]],np]]; (*solving for np*)
		If[npRule!={},matrixElem[[j,3]]=np omegafp/.npRule[[1]], matrixElem[[j,3]]=omegap; matrixElem[[j,1]] = 0]; (*rephrasing the Kronecker delta as delta(omegap, omegap(n1, n2, nf, omegaf)*)
		matrixElem[[j,3]]=matrixElem[[j,3]]/.{n1->n2+(m2-m1)Omega/omegafp,nf->omegaf/omegafp}//FullSimplify; (*adding in energy conservation: n1 omegafp + m1 Omega = n2 omegafp + m2 Omega => n1-n2 = (m2-m1)Omega/omegafp*)
		matrixElem[[j,3]] = matrixElem[[j,3]]/.{Omega->"Omega",omegaf->"omegaf"}; (*variables need to be strings in order not to interfere with variables in the notebook used.*)
		matrixElem[[j,2]]="omegap",
	{j,1,Length[matrixElem]}];
	matrixElem = makeState[matrixElem];
	matrixElem[[;;,1]]=Chop[matrixElem[[;;,1]],threshold]; 
	matrixElem = Sort[makeState[matrixElem],Abs[#1[[1]]]>Abs[#2[[1]]]&]; (*return the different transitions in order of their amplitude*)
	matrixElem[[;;,2]] = matrixElem[[;;,3]];
	matrixElem[[;;,2]] = matrixElem[[;;,2]]/.{"Omega"-> (values[[2F+1]]-values[[2F]]), "omegaf"->omegafundamental};
	matrixElem = matrixElem /. {Omega -> "Omega", omegaf -> "omegaf"};
	matrixElem
]


Options[anyOrder] = options;

anyOrder[m2_, basis_, values_, ord_, opt:OptionsPattern[] ]:=Module[{F = OptionValue[F], m1 = OptionValue[mF], threshold = OptionValue[threshold], omegafundamental = OptionValue[omegaf],
																					shift = OptionValue[shift],
																					order = ord-1, matrixElem = {}, n1, n2, nf, np, vTilde, vTemp, vk, ks, counter, z, omegafp, Omega, 
																					npRule, omegap, omegaf, nz, omegas},	

	If[ord == 1, (*the code below only works for order > 1. ord is the physical order of the transitions, order is ord-1, the variable used for the odometer code*)
		matrixElem = firstOrder[m2, basis, values, opt];
		Return[matrixElem, Module]
	];
	(*find all matrix elements of the transformed interaction and save them to speed the calculation up*)
	vTilde = ConstantArray[0, {2F+1, 2F+1}];
	vTilde = Table[Table[ (*vTilde[m1, m2] = UdVU[m2, m1, ... ] = V_k1k2*)
				UdVU[mk, ml, basis, n1, n2, nf, np, opt], (*<n1, ml|UdVU|n2, mk>*)
				{mk,-F,F}
				],
			{ml,-F,F}];
	(*for nth order, we want to calculate (2F+1)^(n-1) sums of n products of matrix elements each*)
	(*<f|V~^n]|i> = Sum_{k_1,k_2,...,k_n-1}V_fk1 V_k1k2 ... V_k(n-2)k(n-1) V_k(n-1)i 1/(z-E_k1) 1/(z-E_k2) ... 1/(z-E_k(n-1))*)
	(*The following loops generate all possible sets {k1, k2, ..., k(n-1)} and calculate the above product for each of them, adding them all up*)
	ks = ConstantArray[-F, order]; (*{k_1, k_2, ..., k_n-1}={-F,-F,...,-F}, starting point for sum / odometer loop*)
	If[ shift == 0,
		omegas = Table[mF, {mF,-F,F}],
		omegas = values/(values[[2F+1]]-values[[2F]]);
	];
	While[True, (*we stay in this while loop until all combinations of k1, k2, ... k(n-1) are added up*)
		(*matrix multiplication for the current set of ki: V_fk1 V_k1k2 V_k2k3 ... V_kn-2kn-1 V_kn-1i*)
		vTemp = vTilde[[m2+F+1,ks[[1]]+F+1]]; (*V_m2k1*)
		Do[
			vk = vTilde[[ks[[i]]+F+1,ks[[i+1]]+F+1]]; (*V_kik(i+1)*)
			vTemp = Flatten[Table[(*{amp, n1-n2, (n1-n2)(nf, np)} vTemp * V_kik(i+1)*) 
								{1/(z-(n2+vTemp[[l,3]])omegafp - omegas[[ks[[i]]+F+1]] Omega) vTemp[[l,1]]*vk[[j,1]],n1-n2,vTemp[[l,3]]+vk[[j,3]]},
						{l,1,Length[vTemp]},{j,1,Length[vk]}],1],
			{i,1,Length[ks]-1}]; 
			vk = vTilde[[ks[[Length[ks]]]+F+1,m1+F+1]]; (*V_k(n-1)m1*)
			vTemp = Flatten[Table[(*{amp, n1-n2, (n1-n2)(nf, np)} vTemp * V_k(n-1)m1*) 
								{1/(z-(n2+vTemp[[i,3]])omegafp - omegas[[ks[[Length[ks]]]+F+1]] Omega) vTemp[[i,1]]*vk[[j,1]],n1-n2,vTemp[[i,3]]+vk[[j,3]]},
						{i,1,Length[vTemp]},{j,1,Length[vk]}],1];
			AppendTo[matrixElem, vTemp];
		(*the below is to generate all possible combinations of the ks - "Odometer style"*)
		counter = 1;
		ks[[counter]]++;
		While[ks[[counter]]>F,
			ks[[counter]] = -F;
			counter++;
			If[counter > order, Break[]];
			ks[[counter]]++;
		];
		If[counter > order, Break[]]
	];
	matrixElem=Flatten[matrixElem, 1];
	(*Energy conservation determines the frequency of the probe for each transition and with that we can calculate the amplitudes*)
	nz = n1 omegafp + omegas[[m1+F+1]] Omega;
	nz = n2 omegafp + omegas[[m2+F+1]] Omega;
	Clear[vTemp];
	Clear[vTilde];
	Clear[vk];
	Quiet[
		matrixElem[[;;,1]] = matrixElem[[;;,1]]/.z->nz;
 		matrixElem[[;;,1]]=(#[[1]]/.n2->n1-#[[3]])&/@matrixElem;
		npRule = {#, {np} /. Solve[n1 - n2 == #, np]} & /@DeleteDuplicates[matrixElem[[;; , 3]]];
		npRule = Association[#[[1]] -> Flatten[#[[2]]] & /@ npRule];
		matrixElem[[;;,2]] = "omegap";
		matrixElem[[;;,3]] = omegafp np /. np -> npRule[#] &/@ matrixElem[[;; , 3]];
		matrixElem[[;;,3]] = matrixElem[[;;,3,1]];
		matrixElem[[;;,1]] = If[#[[3]]== np omegafp,0,#[[1]],#[[1]]]&/@matrixElem;
		matrixElem[[;;,3]] = matrixElem[[;;,3]]/.{n1->n2+(omegas[[m2+F+1]]-omegas[[m1+F+1]])Omega/omegafp,nf->omegaf/omegafp}//Expand;
		matrixElem[[;;,1]] = matrixElem[[;;,1]] /.np->omegap/omegafp;
		matrixElem[[;;,1]] =( #[[1]]/.omegap->#[[3]])&/@matrixElem;
		matrixElem[[;;,1]] = matrixElem[[;;,1]] /. nf -> omegaf / omegafp;
		matrixElem[[;;,1]] = Simplify[matrixElem[[;;,1]]];
		matrixElem[[;;,1]] = matrixElem[[;;,1]]/.{Omega-> (values[[2F+1]]-values[[2F]]), omegaf->omegafundamental}; (*for symmetric case, values between neighbouring eigenstates are equal to Omega for any states and any F*)
	{Power::infy, Infinity::indet}];
	If[shift != 0,
		matrixElem[[;;,1]] = If[Abs[#[[1]]] > 10^15, 0, #[[1]], #[[1]]] &/@ matrixElem;
	];
	matrixElem = makeState[matrixElem]; (*removing any complexInfinities and indeterminate amplitudes before the threshold is applied*)
	matrixElem = Expand[matrixElem];
	matrixElem[[;;,1]]=Chop[matrixElem[[;;,1]],threshold];
	matrixElem = Simplify[matrixElem];
	matrixElem = makeState[matrixElem];
	matrixElem = Sort[matrixElem,Abs[#1[[1]]]>Abs[#2[[1]]]&];
	matrixElem[[;;,2]] = matrixElem[[;;,3]];
	matrixElem[[;;,2]] = matrixElem[[;;,2]]/.{Omega-> (values[[2F+1]]-values[[2F]]), omegaf->omegafundamental};
	matrixElem = matrixElem /. {Omega -> "Omega", omegaf -> "omegaf"};
	matrixElem
]


(* ::Section:: *)
(*Helper Functions*)


(* ::Text:: *)
(*These helper functions algebraically manipulate states as well as Kronecker deltas. States are represented as lists: We write states (in the eigenbasis of H) in the following way: *)
(*Subscript[a, 1]|Subscript[N, 1],Subscript[m, 1]>+...+Subscript[a, n]|Subscript[N, n],Subscript[m, n]> becomes {{Subscript[a, 1],Subscript[N, 1],Subscript[m, 1]},...,{Subscript[a, n],Subscript[N, n],Subscript[m, n]}}. *)
(*The scalar product returns a matrix which looks the same as those representing states, but representing a sum of Kronecker deltas: *)
(*{{Subscript[a, 1],Subscript[n, 11],Subscript[n, 12]},{Subscript[a, 2],Subscript[n, 21],Subscript[n, 22]},...,{Subscript[a, m],Subscript[n, m1],Subscript[n, m2]}} stands for *)
(*Subscript[a, 1]Delta(Subscript[n, 11],Subscript[n, 12])+Subscript[a, 2]Delta(Subscript[n, 21],Subscript[n, 22])+...+Subscript[a, m]Delta (Subscript[n, m1],Subscript[n, m2]). *)
(*These means the same functions can be used to manipulate both Kronecker deltas and states.*)


(* ::Subsection:: *)
(*Linear Algebra*)


(*finds all duplicate states / Kronecker deltas (where amplitudes are not necessarily the same) in a list and returns their position. List is a list of "states", i.e. three entries each. E.g. {{1,1,1},{1,2,1},{2,1,1}} returns {{1,3},{2}}*)
positionDuplicates[list_]:=GatherBy[Range@Length[list],list[[#,2;;3]]&]


(*adds duplicate states or Kronecker deltas (where amplitudes are not necessarily the same) within a list (i.e. {{amp1, x1, x2}, {amp2, x1, x2}, {amp3, x3, x2}} \[Rule] {{amp1+amp2, x1, x2}, {amp3, x3, x2}}*)
addDuplicates[list_]:=Module[{l=list,pos,output={}},
pos = positionDuplicates[l];
output = {Total[l[[#, 1]]], l[[#[[1]], 2]], l[[#[[1]], 3]]} & /@ pos;
output
]


(*adds duplicates in a state / Kronecker delta, then deletes all states / Kronecker deltas with amplitues of zero or complex infinity*)
makeState[state_]:=Module[{outState = state},
(*remove zeros*)
outState=DeleteCases[outState,{0,_,_}];
outState=DeleteCases[outState,{0.,_,_}];
outState=DeleteCases[outState,{ComplexInfinity,_,_}];
(*accumulate duplicates*)
outState = addDuplicates[outState];
(*remove zeros*)
outState=DeleteCases[outState,{0,_,_}];
outState=DeleteCases[outState,{0.,_,_}];
outState
]


(*multiplies a state / Kronecker delta with a scalar, i.e. {{amp1, x1, y1},{amp2, x2, y2}, ...,{ampn,xn, yn}} \[Rule] {{\[Lambda]amp1, x1, y1}, ..., {\[Lambda]ampn, xn, yn}}*)
scalarMultiply[state_,scalar_]:=makeState[{scalar #[[1]], #[[2]],#[[3]]}&/@state]


(*adds to states/Kronecker deltas*)
addStates[states_]:=Flatten[Join[states],1]


(*returns the scalar product of two states represented as lists and represents it as a list (i.e. Kronecker delta)*)
scalarProduct[state1_,state2_]:=makeState[Flatten[Table[Table[
	{
	If[Expand[KroneckerDelta[state1[[i,2]],state2[[j,2]]]]==0,
		0,
		Conjugate[state1[[i,1]]]*state2[[j,1]]*KroneckerDelta[state1[[i,3]],state2[[j,3]]],Conjugate[state1[[i,1]]]*state2[[j,1]]*KroneckerDelta[state1[[i,3]],state2[[j,3]]]
	],
	state1[[i,2]],state2[[j,2]]},
	{i,1,Length[state1]}],{j,1,Length[state2]}],1]]


(* ::Subsection:: *)
(*Operators*)


(* ::Text:: *)
(*Operators acting on states as described above, acting on the light field or atomic state. These are then combined to form the probe interactions.*)


sigmaP[state_, F_]:={(F(F+1)-#[[3]](#[[3]]+1))^(1/2) #[[1]],#[[2]],#[[3]]+1}&/@state
sigmaM[state_, F_]:={(F(F+1)-#[[3]](#[[3]]-1))^(1/2) #[[1]],#[[2]],#[[3]]-1}&/@state
sigmaZ[state_]:={#[[3]]#[[1]],#[[2]],#[[3]]}&/@state
createPhotons[state_,number_]:={#[[1]],#[[2]]+number,#[[3]]}&/@state
annihilatePhotons[state_,number_]:={#[[1]],#[[2]]-number,#[[3]]}&/@state


(* ::Text:: *)
(*Probe Interactions*)


VpcircPlus[state_,p_, F_]:=addStates[{createPhotons[sigmaM[state, F],p],annihilatePhotons[sigmaP[state, F],p]}]
(*counter-rotating terms neglected in the RWA*)
VpcircMinus[state_,p_, F_]:=addStates[{createPhotons[sigmaP[state, F],p],annihilatePhotons[sigmaM[state, F],p]}]
(*linear polarisation along the direction of the linear dressing RF*)
Vplinx[state_,p_, F_]:=addStates[{VpcircPlus[state,p, F],VpcircMinus[state,p, F]}]
(*linear polarisation perpendicular to circular dressing RF (i.e. vertical in our case)*)
Vpz[state_,p_, F_]:=addStates[{createPhotons[sigmaZ[state],p],annihilatePhotons[sigmaZ[state],p]}]
(*"arbitrary" polarisation in the sense that it finds all possible resonances. Vpliny + Vpz*)
Vparb[state_,p_, F_]:=addStates[{Vplinx[state,p, F],Vpz[state,p, F]}]
(*arbitrary polarisation with amplitudes included*)
VpAmps[state_,p_,F_]:=addStates[{scalarMultiply[VpcircPlus[state,p,F],"Omega+"/2],scalarMultiply[VpcircMinus[state,p,F],"Omega-"/2],scalarMultiply[Vpz[state,p,F],"Omegaz"]}]
(*polarisation as we expect on the experiment, values as used in paper*)
VpExp[state_,p_,F_]:=addStates[{scalarMultiply[VpcircPlus[state,p,F],"0.001"/2],scalarMultiply[VpcircMinus[state,p,F],"0.001"/2],scalarMultiply[Vpz[state,p,F],"0.03"]}]


(* ::Subsection:: *)
(*More Helpers*)


(* ::Text:: *)
(*Some functions called by the functions above...*)


(*prepares the transition data to be plotted, i.e. inserts points with zero amplitude to the left and the right of the peaks*) 
makeSpectrum[list_]:=Module[{l},
	l = ConstantArray[0,{Length[list]*3,2}];
	Do[
		l[[3*i-2]]={list[[i,2]]-0.001,0};
		l[[3*i-1]]={list[[i,2]],Abs[list[[i,1]]]};
		l[[3*i]]={list[[i,2]]+0.001,0},
		{i, 1, Length[list]}
	];
	l
]



(* ::Text:: *)
(*Method that checks common inputs into the Find Transitions functions, returns true if they are all ok and false if they aren't, printing an error message. *)
(*Doesn't test polarisation and probePolarisation, since these are tested later on.*)


Options[checkInputs] = options;

checkInputs::dressingRFsNoList = "Dressing RFs and Rabi Frequencies need to be lists of positive numbers!";
checkInputs::unequalLength = "Dressing RFs and Rabi Frequencies need to be lists of equal length!";
checkInputs::integerDressingRFs = "Dressing RFs need to be positive integers (to obtain dressing RF frequencies, these will be multiplied with omegaf)";
checkInputs::positiveRabiFrequencies = "Rabi Frequencies need to be positive numbers.";
checkInputs::energyNegative = "Energy separation needs to be a positive number";
checkInputs::debugNonBoolean = "Debug needs to be a boolean!";
checkInputs::wrongF = "F needs to be a positive integer (half-integers not yet supported)!";
checkInputs::wrongmF = "mF needs to be a number between -F and F, with integer difference between F and mF!";
checkInputs::wrongNmax = "nMax needs to be a positive integer!";
checkInputs::wrongomegaf = "omegaf needs to be a positive number!";
checkInputs::wrongThreshold = "threshold needs to be a positive number!";
checkInputs::wrongPolarisation = "The polarisation that was specified doesn't exist. Valid options are circularPlus, circularMinus, linear, and linearZ.";
checkInputs::wrongProbePolarisation = "The polarisation that was specified doesn't exist. Valid options are arbitrary, linearY, linearZ, linearX, circularPlus, circularMinus, and amplitudes.";
checkInputs::wrongOmegazAttenuation = "The OmegazAttenuation needs to a real number.";
checkInputs::wrongSigngF = "The sign of gF can only be 1 or -1";
checkInputs::wrongShift = "The shift has to be a real number.";

checkInputs[dressingRFs_, rabiFrequencies_, freqSep_, opt:OptionsPattern[]]:=Module[{debug = OptionValue[Debug], F = OptionValue[F], mF = OptionValue[mF], nmax = OptionValue[nMax], 
																						omegaf = OptionValue[omegaf], threshold = OptionValue[threshold],polarisation = OptionValue[polarisation], probeInteraction = OptionValue[probeInteraction],
																						OmegazAttenuation = OptionValue[OmegazAttenuation], signgF = OptionValue[signgF], shift = OptionValue[shift]},
	(*check dressing RFs and rabi Frequencies*)
	If[Length[dressingRFs]==0, Message[checkInputs::dressingRFsNoList, Style[checkInputs, Red]]; Return[False]];
	If[Length[dressingRFs]!=Length[rabiFrequencies], Message[checkInputs::unequalLength, Style[checkInputs, Red]]; Return[False]];
	If[!AllTrue[dressingRFs, #>0&]|| !AllTrue[dressingRFs, IntegerQ], Message[checkInputs::integerDressingRFs, Style[checkInputs, Red]]; Return[False], True, Message[checkInputs::integerDressingRFs, Style[checkInputs, Red]]; Return[False]];
	If[!AllTrue[rabiFrequencies, #>=0&], Message[checkInputs::positiveRabiFrequencies, Style[checkInputs, Red]]; Return[False], True, Message[checkInputs::positiveRabiFrequencies, Style[checkInputs, Red]]; Return[False]];
	(*check freqSep*)
	If[freqSep<0, Message[checkInputs::energyNegative, Style[checkInputs, Red]]; Return[False], True, Message[checkInputs::energyNegative, Style[checkInputs, Red]]; Return[False]];
	(*check options*)
	If[!BooleanQ[debug], Message[checkInputs::debugNonBoolean, Style[checkInputs, Red]]; Return[False], True, Message[checkInputs::debugNonBoolean, Style[checkInputs, Red]]; Return[False]];
	If[!IntegerQ[F]||F<0, Message[checkInputs::wrongF, Style[checkInputs, Red]]; Return[False], True, Message[checkInputs::wrongF, Style[checkInputs, Red]]; Return[False]];
	If[Abs[mF]>F||!IntegerQ[F-mF], Message[checkInputs::wrongmF, Style[checkInputs, Red]]; Return[False], True, Message[checkInputs::wrongmF, Style[checkInputs, Red]]; Return[False]];
	If[nmax<0||!IntegerQ[nmax], Message[checkInputs::wrongNmax, Style[checkInputs, Red]]; Return[False], True, Message[checkInputs::wrongNmax, Style[checkInputs, Red]]; Return[False]];
	If[omegaf<0||!NumberQ[omegaf], Message[checkInputs::wrongomegaf, Style[checkInputs, Red]]; Return[False], True, Message[checkInputs::wrongomegaf, Style[checkInputs, Red]]; Return[False]];
	If[!NumberQ[threshold]||threshold<0, Message[checkInputs::wrongThreshold, Style[checkInputs, Red]]; Return[False], True, Message[checkInputs::wrongThreshold, Style[checkInputs, Red]]; Return[False]];
	If[!MemberQ[{"circularPlus","circularMinus","linear","linearZ"},polarisation], Message[checkInputs::wrongPolarisation, Style[checkInputs, Red]]; Return[False], True, Message[checkInputs::wrongPolarisation, Style[checkInputs, Red]]; Return[False]];
	If[!MemberQ[{"arbitrary", "circularPlus","circularMinus","linearX","linearY","linearZ","amplitudes","experiment"},probeInteraction], Message[checkInputs::wrongProbePolarisation, Style[checkInputs, Red]]; Return[False], True, Message[checkInputs::wrongProbePolarisation, Style[checkInputs, Red]]; Return[False]];
	If[!NumberQ[OmegazAttenuation], Message[checkInputs::wrongOmegazAttenuation, Style[checkInputs, Red]]; Return[False], True, Message[checkInputs::wrongOmegazAttenuation, Style[checkInputs, Red]]; Return[False]];
	If[!NumberQ[shift], Message[checkInputs::wrongShift, Style[checkInputs, Red]]; Return[False], True, Message[checkInputs::wrongShift, Style[checkInputs, Red]]; Return[False]];
		If[!MemberQ[{-1,1},signgF], Message[checkInputs::wrongSigngF, Style[checkInputs, Red]]; Return[False], True, Message[checkInputs::wrongSigngF, Style[checkInputs, Red]]; Return[False]];
	True		
]


End[]


EndPackage[]
